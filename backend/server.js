const express = require('express');
const cookieParser = require('cookie-parser')
const app = express();

app.post('/', (req, res) => {
  res.cookie('token', '12345ABCDE')
  console.log(req.headers.cookie)
  res.send({ message: 'Hello WWW!' });
});

app.get('/frontend', (req, res) => {
  res.sendfile('./frontend/index.html');
});

app.get('/frontend/index.js', (req, res) => {
  res.sendfile('./frontend/index.js');
});

app.listen(3333, () => {
  console.log('Application listening on port 3333!');
});