const url = '/';

document.addEventListener('DOMContentLoaded', () => {
  //document.cookie = "user=John; max-age=30";
  console.log('cookies', document.cookie);
  fetch(url, {
    credentials: "include",
    method: 'POST'
  })
    .then(data => data.json())
    .then(console.log)

})